# Dockerfile mostly copied from uphold/docker-litecoin-core [1], some parts from tianon/gosu [2]
#
# [1]: https://github.com/uphold/docker-litecoin-core/blob/ac1955bd01a2ead5a9869e1e4045bca04a00af63/0.18/Dockerfile
# [2]: https://github.com/tianon/gosu/blob/23e63902e1ade697a167e7874a5a12243ac0bae8/INSTALL.md

# Use builder image to download and verify litecoin and gosu binaries
FROM debian:stable-slim AS builder

# 1. Set LITECOIN_VERSION as build argument. Allows to specify litecoin version during docker build
# 2. Set GOSU_VERSION as build argument. Allows to specify gosu version during docker build
ARG LITECOIN_VERSION=0.18.1
ARG GOSU_VERSION=1.14

# 1. Update package repository cache, install packages curl, gnupg
# 2. Download gosu's and litecoin's public keys [1]
# 3. Try different keyservers in case one of them is not available
# 4. Download litecoin tar archive
# 5. Download litecoin tar archive signature
# 6. Verify signature using previously downloaded public key
# 7. Calculate sha256 checksum of the tar archive and check that its value is present in the signature
# 8. Unpack the tar archive and strip some unnecessary folders
# 9. Download gosu tar archive
# 10. Download gosu tar archive signature
# 11. Verify downloaded gosu binary with signature (different signature, hence different command)
# 12. Make gosu binary executable
#
# [1]: https://litecointalk.io/t/gpg-file-verification-after-litecoin-download-on-linux-how-to/2681
RUN set -ex \
  && apt-get update -y \
  && apt-get install -y curl gnupg \
  && for key in \
      B42F6819007F00F88E364FD4036A9C25BF357DD4 \
      FE3348877809386C \
    ; do \
      gpg --no-tty --keyserver pgp.mit.edu --recv-keys "$key" || \
      gpg --no-tty --keyserver keyserver.pgp.com --recv-keys "$key" || \
      gpg --no-tty --keyserver ha.pool.sks-keyservers.net --recv-keys "$key" || \
      gpg --no-tty --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" ; \
    done

RUN curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
  && curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
  && gpg --verify litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
  && grep $(sha256sum litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz | awk '{ print $1 }') litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
  && tar --strip=2 -xzf *.tar.gz -C /usr/local/bin \
  && curl -SL -o /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
  && curl -SL -o /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
  && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
  && chmod +x /usr/local/bin/gosu

# Use debian:stable-slim as base image. Not entirely sure why Debian was chosen here. Generally speaking Debian is a
# full-blown OS while Alpine lacks some libraries/utilities which need to be installed manually on a per use-case basis.
# As a result Alpine images are smaller, but Debian images work better 'out-of-the-box'. There are some security
# considerations like Alpine has less software running in it, therefore it has less attack surface. Alpine comes with
# the 'musl' libary which is a C standard library implementation. Debian ships with 'glibc' which is a different
# C standard library implementation. From looking at one (!) post on Reddit [1] regarding this issue, it appears to me
# that 'glibc' is a bit better maintained than 'musl', therefore reducing the risk of critical vulnerabilites
# (or at least a shorter 'time-to-fix'). On the other hand Alpine seems to rank better on vulnerability reports overall,
# which in turn might be a result of a lot of false positives [2].
#
# [1]: https://www.reddit.com/r/docker/comments/77zork/alpine_vs_debianubuntu_securitywise/
# [2]: https://anchore.com/blog/how-many-cves/
#
# TL;DR: Use debian because it supports litecoin out of the box. If it passes the security scanner it is also secure
FROM debian:stable-slim

# Set LITECOIN_DATA as environment variable. It is used in 'docker-entrypoint.sh' to create a folder for litecoin
ENV LITECOIN_DATA=/home/litecoin/.litecoin

# 1. Add normal user 'litecoin' with its own home directory
# 2. Update apt repositories cache
# 3. Upgrade installed apt packages (possibly fixing security vulnerabilites)
# 4. Remove downloaded package archives
# 5. Remove all apt cache and repositories
RUN useradd -m litecoin \
  && apt-get update \
  && apt-get upgrade -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copy litecoin and gosu binaries from builder image
COPY --from=builder /usr/local/bin/litecoind /usr/local/bin/litecoind
COPY --from=builder /usr/local/bin/gosu /usr/local/bin/gosu

# Copy the docker-entrypoint.sh script. Instead of directly starting the litecoin daemon, the entrypoint script will add
# some additional logic before startup - like data folder creation and handling of arguments and most importantly,
# running the litecoin daemon as a user without root privileges
COPY docker-entrypoint.sh /entrypoint.sh

# This will create a folder under /var/lib/docker on the docker host and mount it to the path(s) specified. In this case
# to persist litecoin daemon data
VOLUME ["/home/litecoin/.litecoin"]

# Shows the exposed ports of litecoin daemon. Purely informational though. Binding them to an interface still needs to
# be specified on 'docker run'
EXPOSE 9332 9333 19332 19333 19444

# Command which is run on docker run
ENTRYPOINT ["/entrypoint.sh"]

# Arguments which are passed to the entrypoint command
CMD ["litecoind"]
