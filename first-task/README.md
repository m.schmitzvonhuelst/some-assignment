# First task

Mainly this task was writing comments to what is happening in the [Dockerfile](Dockerfile)
from Uphold. This Dockerfile seems to have everything which is required per the first task, but seemed to lack a few
best practices. Here is what I mainly changed:

1. The original Dockerfile had three different `RUN` directives. I moved downloading, installing and verifying
   of `litecoin` and `gosu` into a build layer. This keeps at least `curl` and `gnupg` out of the final image. I split
   downloading the keys into a separate `RUN` directive, because I had massive problems with this during builds and
   wanted to make sure once I had one image with it I didn't need to build all over again.
2. To me, it looked as if the `gosu` binary was not properly verified. So I fixed that.
3. Changed the user _litecoin_ from a system account to a normal user. Technically almost identical, but the task
   said __normal__ user, so I wanted to be sure.
4. Instead of setting `LITECOIN_VERSION` as an environment variable, I changed it to a build argument. Makes more sense
   to me to specify this as build argument during `docker build`. Same with `GOSU_VERSION`.
5. Updated the `GOSU_VERSION` to its latest release.

## Notes

As mentioned above I had massive issues with fetching the public keys to verify the signatures. Current state is to have
separate `RUN` directives to create separate image layers which I can use as cache. **However** this doesn't work of
course since the layer is in a builder layer, which is not uploaded on push. I will leave it as it is for now, since I
have spent a lot of time already and the build keeps failing because of the keyserver availability.
