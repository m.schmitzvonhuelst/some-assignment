#!/bin/sh

# docker-entrypoint.sh copied from uphold/docker-litecoin-core [1]
#
# [1]: https://github.com/uphold/docker-litecoin-core/blob/ac1955bd01a2ead5a9869e1e4045bca04a00af63/0.18/docker-entrypoint.sh

# Exit script if any command exits with exit code 1
set -ex

# Print out arguments passed to entrypoint command
echo "$@"

# Check if first positional argument is written as an option, e.g. '-help'. If yes, assume this option should be passed to the
# litecoin daemon and set positional arguments to daemon and following options
if [ $(echo "$1" | cut -c1) = "-" ]; then
  echo "$0: assuming arguments for litecoind"

  set -- litecoind "$@"
fi

# Create a data directory based on the environment variable set in the Dockerfile. This may be overridden during docker
# run. Gives user 'litecoin' permission to the newly created folder and appends it as an option to the arguments
if [ $(echo "$1" | cut -c1) = "-" ] || [ "$1" = "litecoind" ]; then
  mkdir -p "$LITECOIN_DATA"
  chmod 770 "$LITECOIN_DATA" || echo "Could not chmod $LITECOIN_DATA (may not have appropriate permissions)"
  chown -R litecoin "$LITECOIN_DATA" || echo "Could not chown $LITECOIN_DATA (may not have appropriate permissions)"

  echo "$0: setting data directory to $LITECOIN_DATA"

  set -- "$@" -datadir="$LITECOIN_DATA"
fi

# Check whether current user is root and first positional argument is supposed to run the litecoin daemon. If yes,
# drop root privileges by running gosu and specifying the user it should run as. At last, pass all positional arguments
# to the command
if [ "$(id -u)" = "0" ] && ([ "$1" = "litecoind" ] || [ "$1" = "litecoin-cli" ] || [ "$1" = "litecoin-tx" ]); then
  set -- gosu litecoin "$@"
fi

# Pass arguments to exec, replacing the shell and effectively running litecoin as main process (pid 1)
exec "$@"
