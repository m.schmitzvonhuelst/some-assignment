# Second task

I couldn't quite find out what the ports `9333` and `19333` are for. However, they work as readiness and liveness
checks. Using the first port `9332` as readiness check kept restarting the pod. Resource requests are not real-world
values since I ran the workload on a cluster setup in GCP and didn't want to spend money on the nodes. In a real world
setting I would run the workload without any constraints and check its average requirements. Then use that as a starting
point.
