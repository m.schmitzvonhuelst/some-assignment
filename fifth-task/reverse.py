#!/bin/env python3

import sys

try:
    arg = sys.argv[1]
    if arg == "-":
        to_reverse = list(input())
    else:
        to_reverse = list(arg)
except IndexError:
    print("Usage:\nEither pass a string as first argument or '-' to read from stdin")
    exit(1)

left = 0
right = len(to_reverse) - 1

while left < right:
    to_reverse[left], to_reverse[right] = to_reverse[right], to_reverse[left]
    left += 1
    right -= 1

print(''.join(to_reverse), end="")
