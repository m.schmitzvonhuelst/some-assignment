# Fifth task

Reverses a string. Came across this recently when learning about algorithms.

## Usage

1. Pipe to script.

```shell
printf "Hello World" | ./reverse.py -
```

2. Pass as argument to script.

```shell
./reverse.py "Hello World"
```
