# some-assignment

![Pipeline Status](https://gitlab.com/m.schmitzvonhuelst/some-assignment/badges/main/pipeline.svg)

[TOC]

## [First task](first-task/README.md)

## [Second task](second-task/README.md)

## [Third task](third-task/README.md)

## [Fourth task](fourth-task/README.md)

## [Fifth task](fifth-task/README.md)

## [Sixth task](sixth-task/README.md)
