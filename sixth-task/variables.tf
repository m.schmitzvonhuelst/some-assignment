variable "gcp_project_id" {
  type        = string
  description = "Id of the project for which the resources should be created."
  default     = "temptek"
}

variable "gcp_project_region" {
  type        = string
  description = "Region of the project for which the resources should be created."
  default     = "europe-west3"
}

variable "gcp_custom_role" {
  type        = object({
    role_id     = string
    title       = string
    description = string
  })
  description = "Custom role object."
  default     = {
    role_id     = "ciRole"
    title       = "CI Role"
    description = "Role for continuous integration user."
  }
}

variable "gcp_service_account" {
  type        = object({
    account_id   = string
    display_name = string
  })
  description = "Service account object."
  default     = {
    account_id   = "ci-service-account"
    display_name = "Service Account for continuous integration."
  }
}

variable "set_random_suffix" {
  type        = bool
  description = "Set to add a random suffix to ressource names."
  default     = false
}
