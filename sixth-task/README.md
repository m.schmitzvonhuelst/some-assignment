# Sixth task

I am not entirely sure about this task. I only have experience with GCP and GCP doesn't allow creating roles without
permissions. I also couldn't find any resource to create a user. Instead, I created a service account for the
assignment. Also, GCP has groups, but not in the way I believe it is asked for in the task.

The starting point is the [example](example/terraform.tf).
