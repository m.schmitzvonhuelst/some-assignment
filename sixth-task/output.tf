output "ci_custom_role_id" {
  value = google_project_iam_custom_role.ci_role.id
}

output "ci_service_account_email" {
  value = google_service_account.ci_service_account.email
}
