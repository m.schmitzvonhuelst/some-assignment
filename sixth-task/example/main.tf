module "custom_role_and_user" {
  source = "../"

  gcp_custom_role = {
    role_id     = "anotherCiRole"
    title       = "Another CI Role"
    description = "Another role for continuous integration user."
  }

  gcp_service_account = {
    account_id   = "another-ci-service-account"
    display_name = "Another Service Account for continuous integration."
  }

  set_random_suffix = true
}
