resource "random_id" "suffix" {
  byte_length = 4
}

resource "google_service_account" "ci_service_account" {
  account_id   = var.set_random_suffix ? substr("${var.gcp_service_account.account_id}-${random_id.suffix.hex}", 0, 28) : var.gcp_service_account.account_id
  display_name = var.gcp_service_account.display_name
}

resource "google_project_iam_custom_role" "ci_role" {
  role_id     = var.set_random_suffix ? "${var.gcp_custom_role.role_id}${random_id.suffix.hex}" : var.gcp_custom_role.role_id
  title       = var.gcp_custom_role.title
  description = var.gcp_custom_role.description
  permissions = ["container.clusters.list"]
}

resource "google_project_iam_policy" "ci_project_policy" {
  project     = var.gcp_project_id
  policy_data = data.google_iam_policy.ci_policy.policy_data
}

data "google_iam_policy" "ci_policy" {
  binding {
    role    = "roles/owner"
    members = [
      "user:m.schmitzvonhuelst@gmail.com",
    ]
  }

  binding {
    role    = "roles/editor"
    members = [
      "serviceAccount:gitlab-ci@temptek.iam.gserviceaccount.com"
    ]
  }

  binding {
    role    = "roles/container.clusterAdmin"
    members = [
      "serviceAccount:gitlab-ci@temptek.iam.gserviceaccount.com"
    ]
  }

  binding {
    role    = "roles/container.admin"
    members = [
      "serviceAccount:gitlab-ci@temptek.iam.gserviceaccount.com"
    ]
  }

  binding {
    role    = google_project_iam_custom_role.ci_role.id
    members = ["serviceAccount:${google_service_account.ci_service_account.email}"]
  }
}
