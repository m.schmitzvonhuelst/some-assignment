# Fourth task

The most difficult task of them all. To come up with a more interesting problem than just substituting a string. I had
to look it up. The solution wasn't too complicated though.

## Usage

```shell
printf "Hello World" | ./reverse.sh
```
