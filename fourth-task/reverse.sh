#!/bin/sh

# There is an insane implementation on the sed man page [1], but I don't understand the use of labels.
# The below was easier for me, since it uses commands I have used before. Found here [2].
#
# [1]: https://www.gnu.org/software/sed/manual/sed.html#Reverse-chars-of-lines
# [2]: https://www.folkstalk.com/2011/12/methods-to-reverse-string-using-unix.html

# First substitute given input by appending a newline behind every character found in input
# Second, just like cat, tac concatenates lines, but reversed
# Third, trim the previously added newlines again
sed 's/./&\n/g' \
  | tac \
  | tr -d '\n'
