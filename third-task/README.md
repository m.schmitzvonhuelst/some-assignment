# Third task

![Pipeline Status](https://gitlab.com/m.schmitzvonhuelst/some-assignment/badges/main/pipeline.svg)

For this I decided to just host the repository on Gitlab to make use of the pipeline features. Mainly there are three
steps and all steps should only run on changes to `Dockerfile` and `docker-entrypoint.sh`:

1. Build, scan and push the docker image to the Gitlab container registry.
2. Fetch kubeconfig via the gcloud command, for a cluster hosted in GCP.
3. Replace the image inside the StatefulSet with the newly built image and then run `kubectl apply -f`.

## Notes

I tried to implement a job which tags the repository after new docker images have been pushed. However, tags trigger new
pipeline runs, and I decided to skip this for the assignment. What I want to say is, I would tag the repo after
publishing a new docker image. This allows to somehow reproduce certain build tags.

